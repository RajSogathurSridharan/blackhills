#pragma checksum "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d910be1cf39b5a1faa6230144bea7e6bbf9bacea"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Facilities__FacilityPartial), @"mvc.1.0.view", @"/Views/Facilities/_FacilityPartial.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\_dev\BlackHills\Views\_ViewImports.cshtml"
using BlackHills;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_dev\BlackHills\Views\_ViewImports.cshtml"
using BlackHills.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d910be1cf39b5a1faa6230144bea7e6bbf9bacea", @"/Views/Facilities/_FacilityPartial.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2505b3574a7701da5d52f09176dc18b99713e3db", @"/Views/_ViewImports.cshtml")]
    public class Views_Facilities__FacilityPartial : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<BlackHills.ViewModel.FacilitiesViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("FacilitiesForm"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", new global::Microsoft.AspNetCore.Html.HtmlString("FacilitiesForm"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Facilities", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "SaveFacility", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
  
    ViewData["Title"] = @ViewBag.TitleMessage;

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<style>\r\n\r\n    #popup1 {\r\n        z-index: 9997;\r\n    }\r\n\r\n    #errorDialog {\r\n        z-index: 9998;\r\n    }\r\n\r\n    #long-textbox {\r\n        width: 300px;\r\n    }\r\n\r\n    .editorBig {\r\n        width: 400px;\r\n        height: 30px;\r\n    }\r\n</style>\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d910be1cf39b5a1faa6230144bea7e6bbf9bacea5319", async() => {
                WriteLiteral("\r\n    <input type=\"hidden\" id=\"displayStatus\"");
                BeginWriteAttribute("value", " value=\"", 515, "\"", 523, 0);
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <input type=\"hidden\" id=\"selectedOption\"");
                BeginWriteAttribute("value", " value=\"", 573, "\"", 581, 0);
                EndWriteAttribute();
                WriteLiteral(" />\r\n    \r\n    <div class=\"modal-content\" id=\"popup1\">\r\n        <div class=\"modal-header text-center\">\r\n            <h4 class=\"modal-title text-center\" id=\"editFacility\" text-align=\"center\">");
#nullable restore
#line 32 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
                                                                                 Write(ViewBag.TitleMessage);

#line default
#line hidden
#nullable disable
                WriteLiteral("</h4>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">\r\n                <span>x</span>\r\n            </button>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            <div class=\"form-group\">\r\n                ");
#nullable restore
#line 39 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
           Write(Html.HiddenFor(model => model.FacilityId));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                ");
#nullable restore
#line 40 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
           Write(Html.HiddenFor(model => model.isActive));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                ");
#nullable restore
#line 41 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
           Write(Html.HiddenFor(model => model.facilitiesNamesList));

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
                <div class=""row"">
                    <div class=""col-md-3""></div>
                    <div class=""col-md-9""><span id=""errorMessage"" style=""color:red""></span></div>
                    <div class=""col-md-12"">
                        <div class=""form-group editor editorMediumText"">
                            <label>Facility Name</label>
                            ");
#nullable restore
#line 48 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
                       Write(Html.TextBoxFor(M => M.FacilityName, new { style = "width:400px; height:30px;", title="Max 100 characters allowed" }));

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
                        </div>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-md-6"">
                        <div class=""form-group editor editorMediumText"">
                            <input type=""submit"" id=""SaveEditFacility"" value=""Save"" class=""btn btn-primary"" data-dismiss=""modal"" data-toggle=""modal"" href=""#errorDialog"">
                            <input type=""button"" class=""btn btn-primary"" data-dismiss=""modal"" value=""Cancel"">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n<script>\r\n    var facilitiesMasterList = ");
#nullable restore
#line 67 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
                          Write(Html.Raw(Json.Serialize(Model.facilitiesNamesList)));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n    $(document).ready(function () {\r\n\r\n        $(\"#errorDialog\").dialog({\r\n            autoOpen: false,\r\n            modal: true,\r\n            draggable: true\r\n        });\r\n\r\n        if (\'");
#nullable restore
#line 76 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
        Write(ViewBag.selectedOption);

#line default
#line hidden
#nullable disable
            WriteLiteral("\' != \"\") {\r\n            $(\"#selectedOption\").val(\'");
#nullable restore
#line 77 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
                                 Write(ViewBag.selectedOption);

#line default
#line hidden
#nullable disable
            WriteLiteral("\');\r\n            }\r\n\r\n            if (\'");
#nullable restore
#line 80 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
            Write(ViewBag.displayStatus);

#line default
#line hidden
#nullable disable
            WriteLiteral("\' != \"\") {\r\n                $(\"#displayStatus\").val(\'");
#nullable restore
#line 81 "C:\_dev\BlackHills\Views\Facilities\_FacilityPartial.cshtml"
                                    Write(ViewBag.displayStatus);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"');
        }

        $('#FacilityName').on('keypress', function (event) {
            var regex = new RegExp(""^[a-zA-Z0-9 /-]+$"");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });
    });

    $('#SaveEditFacility').click(function (e) {

        if ($(""#FacilityName"").val().trim() == """" || $(""#FacilityName"").val().length == 0) {
            $(""#errorMessage"").text(""Please Enter Facility Name"");
            return false;
        }

        var facName = $(""#FacilityName"").val().trim();
        var facilityExists = false;
        for (var i=0;i<facilitiesMasterList.length;i++){
            if (facName == facilitiesMasterList[i]) {
                facilityExists = true;
                break;
            }
        }
        if (facilityExists) {
            $(""#errorMessage"").text(""Facility Name already exists.");
            WriteLiteral(@""");
            return false;
        }
            var status = $(""#displayStatus"").val();
        var selectedOption = $(""#selectedOption"").val();
        var input = $(""<input>"")
            .attr(""type"", ""hidden"")
            .attr(""name"", ""displayStatus"").val(status);
        $('#FacilitiesForm').append(input);

        var input1 = $(""<input>"")
            .attr(""type"", ""hidden"")
            .attr(""name"", ""selectedOption"").val(selectedOption);
        $('#FacilitiesForm').append(input1);
        $(""#FacilitiesForm"").submit();
    })
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<BlackHills.ViewModel.FacilitiesViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
