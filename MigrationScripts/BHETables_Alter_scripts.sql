USE Digital_Library_Dev
ALTER TABLE [dbo].[Facilities]
ALTER  COLUMN FacilityName nvarchar(100) NOT NULL 

ALTER TABLE [dbo].[Units]
ALTER  COLUMN UnitName nvarchar(100) NOT NULL 

ALTER TABLE [dbo].[Drawings]
ALTER  COLUMN DrawingName nvarchar(100) NOT NULL 


USE Digital_Library_Test
ALTER TABLE [dbo].[Facilities]
ALTER  COLUMN FacilityName nvarchar(100) NOT NULL 

ALTER TABLE [dbo].[Units]
ALTER  COLUMN UnitName nvarchar(100) NOT NULL 

ALTER TABLE [dbo].[Drawings]
ALTER  COLUMN DrawingName nvarchar(100) NOT NULL 
