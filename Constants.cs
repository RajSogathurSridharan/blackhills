﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalLibrary
{
    public static class Constants
    {   
        public static string GeneralExceptionUserMessage = "Application is not reachable. Please contact Administrator for assistance.";
        public static string GeneralExceptionAdminMessage = "Please contact the technical support to look at the log & fix the issue.";

        public static string FolderNotFoundAdminMessage = "Folder does not exist. Please verify link path and upload file location.";
        public static string FolderNotFoundUserMessage = "Folder does not exist. Please Email:  BHEPowerGenerationDocumentControls@BlackhillsEnergy.com for assistance.";

        public static string FileNotFoundAdminMessage = "File does not exist. Please verify link path and upload file location.";
        public static string FileNotFoundUserMessage = "File does not exist. Please Email: BHEPowerGenerationDocumentControls@BlackhillsEnergy.com for assistance.";

        public static bool isAdmin = false;

        public static string LinkedPIDs= "";
    }
}
