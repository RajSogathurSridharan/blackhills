﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;


using dm = BlackHills.Models;
using vm = BlackHills.ViewModel;
namespace BlackHills.AutoMapperProfiles
{
    public class BlackhillDrawings : Profile
    {
        public BlackhillDrawings()
        {
            CreateMap<dm.Drawings, vm.DrawingsViewModel>();
        }
    }
}
