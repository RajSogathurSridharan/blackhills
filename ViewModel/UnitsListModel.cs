﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackHills.ViewModel
{
    public class UnitsListModel
    {
        public ICollection<UnitsViewModel> Data { get; set; }
    }
}
