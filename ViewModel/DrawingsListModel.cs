﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackHills.Models;

namespace BlackHills.ViewModel
{
    public class DrawingsListModel
    {
        public string searchDrawingName { get; set; }

        public string searchSystemDesc { get; set; }
        public ICollection<DrawingsViewModel> Data { get; set; }

        public IEnumerable<SelectListItem> Facilities { get; set; }

        public int? FacilityId { get; set; }

        public IEnumerable<SelectListItem> UnitsList { get; set; }

        public int? UnitId { get; set; }

        public string SearchDrawingName { get; set; }

        public string SearchSystemDesc { get; set; }

        public string SearchUnit { get; set; }

    }
}
