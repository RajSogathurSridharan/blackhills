﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BlackHills.ViewModel
{
    public class DrawingsViewModel
    {
        public int DrawingId { get; set; }

        [MaxLength(100)]
        [RegularExpression(@"^[a-zA-Z0-9 /-]+$", ErrorMessage = "Special Characters are not allowed.")]
        [Required]
        public string DrawingName { get; set; }

        [MaxLength(255)]
        public string SystemDescription { get; set; }

        [MaxLength(255)]
        public string FilePath { get; set; }

        [MaxLength(100)]
        public string FileName { get; set; }

        public int UnitId { get; set; }

        [MaxLength(100)]
        public string UnitDesc { get; set; }

        public int FacilityId { get; set; }

        [MaxLength(100)]
        public string FacilityDesc { get; set; }

        public List<SelectListItem> ListFacilities { get; set; }

        public List<SelectListItem> ListUnits { get; set; }

        public IFormFile DrawingPDFFile { get; set; }

        public bool isActive { get; set; }

        [MaxLength(100)]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Special Characters are not allowed.")]
        public string selectedOption { get; set; }

        public bool unitStatus { get; set; }

        public List<string> drawingsMasterList { get; set; }

    }
}
