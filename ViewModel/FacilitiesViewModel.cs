﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlackHills.ViewModel
{
    public class FacilitiesViewModel
    {
        public int FacilityId { get; set; }

        [MaxLength(100)]
        [RegularExpression(@"^[a-zA-Z0-9 /-]+$", ErrorMessage = "Special Characters are not allowed.")]
        [Required]
        public string FacilityName { get; set; }

        public bool deleteEnabledFlag { get; set; }

        public bool isActive { get; set; }

        public bool displayActive { get; set; }

        public List<string> facilitiesNamesList { get; set; }

    }
}
