﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackHills.ViewModel
{
    public class FacilitiesListModel
    {
        public ICollection<FacilitiesViewModel> Data { get; set; }

    }
}
