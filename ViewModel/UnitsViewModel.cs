﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackHills.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace BlackHills.ViewModel
{
    public class UnitsViewModel
    {
        public int UnitId { get; set; }

        [MaxLength(100)]
        [RegularExpression(@"^[a-zA-Z0-9 /-]+$", ErrorMessage = "Special Characters are not allowed.")]
        [Required]
        public string UnitName { get; set; }

        [MaxLength(100)]
        [RegularExpression(@"^[a-zA-Z0-9 /-]+$", ErrorMessage = "Special Characters are not allowed.")]
        public string FacilityName { get; set; }

        public int FacilityId { get; set; }

        public List<SelectListItem> ListFacilities { get; set; }

        public bool deleteEnabledFlag { get; set; }

        public bool isActive { get; set; }

        [MaxLength(100)]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Special Characters are not allowed.")]
        public string selectedOption { get; set; }

        public bool facilityStatus { get; set; }

        public List<string> unitsMasterList { get; set; }

    }
}
