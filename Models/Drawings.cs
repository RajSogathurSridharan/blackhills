﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace BlackHills.Models
{
    [Table("Drawings")]
    public class Drawings
    {
        [Key]
        public int DrawingId { get; set; }

        public string DrawingName { get; set; }

        public string SystemDescription { get; set; }

        public string FilePath { get; set; }

        public string FileName { get; set; }

        public int UnitId { get; set; }

        public bool isActive { get; set; }
    }
}
