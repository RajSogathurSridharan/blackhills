﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace BlackHills.Models
{
    [Table("Units")]
    public class Units
    {
        [Key]
        public int UnitId { get; set; }

        public string UnitName { get; set; }

        public int FacilityId { get; set; }

        public bool isActive { get; set; }
    }
}
