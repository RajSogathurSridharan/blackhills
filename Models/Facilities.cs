﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BlackHills.Models
{
    [Table("Facilities")]
    public class Facilities
    {
        [Key]
        public int FacilityId { get; set; }

        public string FacilityName { get; set; }

        public bool isActive { get; set; }
    }
}
