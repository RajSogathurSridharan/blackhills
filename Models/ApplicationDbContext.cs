﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BlackHills.Models;

namespace BlackHills.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Drawings> Drawings { get; set; }
        public DbSet<Facilities> Facilities { get; set; }
        public DbSet<Units> Units { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Drawings>()
                .HasKey(M => new { M.DrawingId });

            modelBuilder.Entity<Facilities>()
                .HasKey(M => new { M.FacilityId});

            modelBuilder.Entity<Units>()
                .HasKey(M => new { M.UnitId });
        }

        }
}
