﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlackHills.Models;
using BlackHills.ViewModel;
using DigitalLibrary;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

namespace BlackHills.Controllers
{
    public class DrawingsController : Controller
    {

        private readonly ApplicationDbContext _DrawingsDbContext;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly ILogger<DrawingsController> _logger;

        public DrawingsController(ApplicationDbContext projectDbContext, IMapper mapper, IWebHostEnvironment hostEnvironment, ILogger<DrawingsController> logger)
        {
            _DrawingsDbContext = projectDbContext;
            _mapper = mapper;
            webHostEnvironment = hostEnvironment;
            _logger = logger;
        }

        public IActionResult Index(string status, string selection)
        {
            DrawingsListModel DLM = new DrawingsListModel();
            try
            {
                
                var DrawingsListResult = (from d in _DrawingsDbContext.Drawings
                                          join u in _DrawingsDbContext.Units
                                          on d.UnitId equals u.UnitId
                                          join f in _DrawingsDbContext.Facilities
                                          on u.FacilityId equals f.FacilityId
                                          select new DrawingsViewModel()
                                          {
                                              DrawingId = d.DrawingId,
                                              DrawingName = d.DrawingName,
                                              SystemDescription = d.SystemDescription,
                                              FilePath = d.FilePath,
                                              UnitId = d.UnitId,
                                              UnitDesc = u.UnitName,
                                              FacilityId = f.FacilityId,
                                              FacilityDesc = f.FacilityName,
                                              isActive = d.isActive,
                                              unitStatus = u.isActive
                                          }).AsQueryable();



                if (status == null)
                {
                    status = "true";
                }

                DrawingsListResult = DrawingsListResult.Where(D => D.isActive == Boolean.Parse(status));

                var DrawingResultsFiltered = DrawingsListResult.ToList();

                var vm = from i in DrawingResultsFiltered
                         select _mapper.Map<DrawingsViewModel>(i);
                DLM.Data = vm.OrderBy(d => d.FacilityDesc).ThenBy(d => d.UnitDesc).ThenBy(d => d.DrawingName).ToList();

            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            ViewBag.selectedOption = selection;
            ViewBag.displayStatus = status;
            return View(DLM);
        }

        public IActionResult AddDrawing(string displayStatus, string selection)
        {
             DrawingsViewModel DVM = new DrawingsViewModel();
            try
            {
                List<SelectListItem> facilitiesList = _DrawingsDbContext.Facilities.Where(F => F.isActive == true).ToList().ConvertAll(a =>
                              new SelectListItem()
                              {
                                  Text = a.FacilityName.ToString(),
                                  Value = a.FacilityId.ToString(),
                              }
                      );

                var drawingsList = (from draw in _DrawingsDbContext.Drawings
                                    select new string(draw.UnitId + ";" + draw.DrawingName))
                            .ToList();
                DVM.drawingsMasterList = drawingsList;
                DVM.ListFacilities = facilitiesList;
                List<SelectListItem> unitsList = new List<SelectListItem>();
                DVM.ListUnits = unitsList;
                DVM.isActive = true;
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            ViewBag.TitleMessage = "Add Drawing";
            ViewBag.selectedOption = selection;
            ViewBag.displayStatus = displayStatus;
            return PartialView("~/Views/Drawings/_DrawingsPartial.cshtml", DVM);
        }
        public IActionResult LoadUnitsDropdown(int id)
        {
            int facId =  id;
            List<SelectListItem> unitsList = new List<SelectListItem>();

            try
            {
                if (facId != 0)
                {
                    unitsList = _DrawingsDbContext.Units.Where(u => u.FacilityId == facId && u.isActive == true).ToList().ConvertAll(a =>
                          new SelectListItem()
                          {
                              Text = a.UnitName.ToString(),
                              Value = a.UnitId.ToString(),
                          }
                    );
                }
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }

            return Json(unitsList);
        }

        public IActionResult EditDrawing(string drawingId, string displayStatus, string selection)
        {
            DrawingsViewModel dvm = new DrawingsViewModel();
            try
            {
                var editDrawing = (from d in _DrawingsDbContext.Drawings
                                   join u in _DrawingsDbContext.Units
                                   on d.UnitId equals u.UnitId
                                   join f in _DrawingsDbContext.Facilities
                                   on u.FacilityId equals f.FacilityId
                                   where d.DrawingId == Convert.ToInt32(drawingId)
                                   select new DrawingsViewModel()
                                   {
                                       DrawingId = d.DrawingId,
                                       DrawingName = d.DrawingName,
                                       SystemDescription = d.SystemDescription,
                                       FilePath = d.FilePath,
                                       FileName = d.FileName,
                                       UnitId = d.UnitId,
                                       UnitDesc = u.UnitName,
                                       FacilityId = f.FacilityId,
                                       FacilityDesc = f.FacilityName,
                                       isActive = d.isActive
                                   }).FirstOrDefault();
                dvm.DrawingId = editDrawing.DrawingId;
                dvm.DrawingName = editDrawing.DrawingName;
                List<SelectListItem> facilitiesList = _DrawingsDbContext.Facilities.Where(F => F.isActive == true).ToList().ConvertAll(a =>
                        new SelectListItem()
                        {
                            Text = a.FacilityName.ToString(),
                            Value = a.FacilityId.ToString(),
                        }
                );
                List<SelectListItem> unitsList = _DrawingsDbContext.Units.Where(u => u.FacilityId == editDrawing.FacilityId && u.isActive == true).ToList().ConvertAll(a =>
                           new SelectListItem()
                           {
                               Text = a.UnitName.ToString(),
                               Value = a.UnitId.ToString(),
                           }
                     );
                dvm.ListFacilities = facilitiesList;
                dvm.FacilityId = editDrawing.FacilityId;
                dvm.ListUnits = unitsList;
                dvm.UnitId = editDrawing.UnitId;
                dvm.SystemDescription = editDrawing.SystemDescription;
                dvm.FilePath = editDrawing.FilePath;
                dvm.FileName = editDrawing.FileName;
                var drawingsList = (from draw in _DrawingsDbContext.Drawings
                                    where (draw.DrawingId != int.Parse(drawingId))
                                    select new string(draw.UnitId + ";" + draw.DrawingName))
                            .ToList();
                dvm.drawingsMasterList = drawingsList;
                dvm.isActive = editDrawing.isActive;
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            ViewBag.TitleMessage = "Edit Drawing";
            ViewBag.selectedOption = selection;
            ViewBag.displayStatus = displayStatus;
            return PartialView("~/Views/Drawings/_DrawingsPartial.cshtml", dvm);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveDrawing(DrawingsViewModel drawingsModel, string displayStatus, string selectedOption)
        {
            bool validModel = ModelState.IsValid;
            if (validModel)
            {
                try
                {
                    if (drawingsModel.DrawingPDFFile?.Length > 0)
                    {
                        var basePath = Constants.LinkedPIDs;
                        var fileName = drawingsModel.DrawingPDFFile.FileName;
                        var fullPath = Path.Combine(basePath, fileName);
                        using (var fileStream = new FileStream(fullPath, FileMode.Create))
                        {
                            drawingsModel.DrawingPDFFile.CopyTo(fileStream);
                        }
                        drawingsModel.FilePath = fullPath;
                        drawingsModel.FileName = fileName;
                    }

                    if (drawingsModel.DrawingId != 0)
                    {
                        Drawings drawings = _DrawingsDbContext.Drawings.Where(d => d.DrawingId == drawingsModel.DrawingId).FirstOrDefault();
                        drawings.DrawingName = drawingsModel.DrawingName;
                        drawings.SystemDescription = drawingsModel.SystemDescription;
                        drawings.UnitId = drawingsModel.UnitId;
                        drawings.isActive = drawingsModel.isActive;
                        if (drawingsModel.FilePath != null)
                        {
                            drawings.FilePath = drawingsModel.FilePath;
                            drawings.FileName = drawingsModel.FileName;
                        }
                        _DrawingsDbContext.SaveChanges();
                    }
                    else
                    {
                        Drawings drawings = new Drawings();
                        drawings.DrawingName = drawingsModel.DrawingName;
                        drawings.SystemDescription = drawingsModel.SystemDescription;
                        drawings.UnitId = drawingsModel.UnitId;
                        drawings.isActive = drawingsModel.isActive;
                        if (drawingsModel.FilePath != null)
                        {
                            drawings.FilePath = drawingsModel.FilePath;
                            drawings.FileName = drawingsModel.FileName;
                        }
                        _DrawingsDbContext.Add(drawings);
                        _DrawingsDbContext.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    if (Constants.isAdmin)
                    {
                        _logger.LogError(ex.ToString());
                        return Content(Constants.GeneralExceptionAdminMessage);
                    }
                    else
                    {
                        _logger.LogError(ex.ToString());
                        return Content(Constants.GeneralExceptionUserMessage);
                    }
                }
            }
            ViewBag.selectedOption = selectedOption;
            ViewBag.displayStatus = displayStatus;
            return RedirectToAction("Index", "Drawings", new { status = displayStatus, selection = selectedOption });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteDrawing(string drawingId, string displayStatus, string selection)
        {
            try
            {
                int drawId = Convert.ToInt32(drawingId);
                if (drawId != 0)
                {
                    Drawings drawings = _DrawingsDbContext.Drawings.Where(f => f.DrawingId == drawId).FirstOrDefault();
                    _DrawingsDbContext.Remove(drawings);
                    _DrawingsDbContext.SaveChanges();
                    if (drawings.FilePath != null)
                    {
                        System.IO.File.Delete(drawings.FilePath);
                    }

                }
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            ViewBag.selectedOption = selection;
            ViewBag.displayStatus = displayStatus;
            return RedirectToAction("Index", "Drawings", new { status = displayStatus, selection = selection });
        }

        public IActionResult ActivateDrawing(string drawingId, string status, string selection)
        {
            try
            {
                int drawId = Convert.ToInt32(drawingId);
                if (drawId != 0)
                {
                    Drawings drawings = _DrawingsDbContext.Drawings.Where(f => f.DrawingId == drawId).FirstOrDefault();
                    drawings.isActive = true;
                    _DrawingsDbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            return RedirectToAction("Index", "Drawings", new { status = status, selection = selection });
        }

        public IActionResult InactivateDrawing(string drawingId, string status, string selection)
        {
            try
            {
                int drawId = Convert.ToInt32(drawingId);
                if (drawId != 0)
                {
                    Drawings drawings = _DrawingsDbContext.Drawings.Where(f => f.DrawingId == drawId).FirstOrDefault();
                    drawings.isActive = false;
                    _DrawingsDbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            return RedirectToAction("Index", "Drawings", new {status= status, selection = selection });
        }
    }
}
