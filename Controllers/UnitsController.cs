﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlackHills.Models;
using BlackHills.ViewModel;
using DigitalLibrary;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

namespace BlackHills.Controllers
{
    public class UnitsController : Controller
    {

        private readonly ApplicationDbContext _UnitsDbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<UnitsController> _logger;

        public UnitsController(ApplicationDbContext projectDbContext, IMapper mapper, ILogger<UnitsController> logger)
        {
            _UnitsDbContext = projectDbContext;
            _mapper = mapper;
            _logger = logger;
        }

        public IActionResult Index(string status, string selection)
        {
            UnitsListModel ULM = new UnitsListModel();
            try
            {
                // query to pull only the facility List that have associated drawings
                var unitsListWithDrawings = (from unit in _UnitsDbContext.Units
                                             join draw in _UnitsDbContext.Drawings
                                             on unit.UnitId equals draw.UnitId
                                             select new UnitsViewModel { UnitId = unit.UnitId, UnitName = unit.UnitName, isActive = unit.isActive }).ToList();

                // query to identify if the facilities has drawings or not and set the deleteEnabledFlag accordingly in the facilityList
                var unitFinalList = (from unit in _UnitsDbContext.Units.ToList()
                                     join facility in _UnitsDbContext.Facilities
                                     on unit.FacilityId equals facility.FacilityId
                                     join unitObjWD in unitsListWithDrawings
                                     on unit.UnitId equals unitObjWD.UnitId
                                     into unitObject
                                     from unitObj in unitObject.DefaultIfEmpty()
                                     select new UnitsViewModel { UnitId = unit.UnitId, UnitName = unit.UnitName, FacilityName = facility.FacilityName, deleteEnabledFlag = unitObj != null ? false : true, isActive = unit.isActive, facilityStatus = facility.isActive }).GroupBy(u => u.UnitId)
                                         .Select(g => g.FirstOrDefault()).OrderBy(F => F.FacilityName).ThenBy(F => F.UnitName).ToList();

                if (status == null)
                {
                    status = "true";
                }

                var unitssFilteredList = unitFinalList.Where(D => D.isActive == bool.Parse(status)).ToList();

                ULM.Data = unitssFilteredList;
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            ViewBag.selectedOption = selection;
            ViewBag.displayStatus = status;
            return View(ULM);
        }

        
                public IActionResult AddUnit(string displayStatus, string selection)
                {
                    UnitsViewModel UVM = new UnitsViewModel();
                    try
                    {
                        List<SelectListItem> facilitiesList = _UnitsDbContext.Facilities.Where(F => F.isActive == true).ToList().ConvertAll(a =>
                             new SelectListItem()
                             {
                                 Text = a.FacilityName.ToString(),
                                 Value = a.FacilityId.ToString(),
                             }
                       );


                        var unitsList = (from unit in _UnitsDbContext.Units
                                         select new string(unit.FacilityId + ";" + unit.UnitName))
                             .ToList();
                        UVM.unitsMasterList = unitsList;
                        UVM.ListFacilities = facilitiesList;
                        UVM.isActive = true;
                    }
                    catch (Exception ex)
                    {
                        if (Constants.isAdmin)
                        {
                            _logger.LogError(ex.ToString());
                            return Content(Constants.GeneralExceptionAdminMessage);
                        }
                        else
                        {
                            _logger.LogError(ex.ToString());
                            return Content(Constants.GeneralExceptionUserMessage);
                        }
                    }
                    ViewBag.TitleMessage = "Add Unit";
                    ViewBag.selectedOption = selection;
                    ViewBag.displayStatus = displayStatus;
                    return PartialView("~/Views/Units/_UnitPartial.cshtml", UVM);
                }

                public IActionResult EditUnit(string UnitId, string displayStatus, string selection)
                {
                    UnitsViewModel uvm = new UnitsViewModel();
                    try
                    {
                        var editUnit = _UnitsDbContext.Units.Where(f => f.UnitId == Convert.ToInt32(UnitId)).FirstOrDefault();
                        uvm.UnitId = Convert.ToInt32(UnitId);
                        uvm.UnitName = editUnit.UnitName;
                        List<SelectListItem> facilitiesList = _UnitsDbContext.Facilities.Where(F => F.isActive == true).OrderBy(F => F.FacilityName).ToList().ConvertAll(a =>
                                 new SelectListItem()
                                 {
                                     Text = a.FacilityName.ToString(),
                                     Value = a.FacilityId.ToString(),
                                 }
                        );
                        uvm.FacilityId = editUnit.FacilityId;
                        uvm.ListFacilities = facilitiesList;
                        var unitsList = (from unit in _UnitsDbContext.Units
                                         where (unit.UnitId != int.Parse(UnitId))
                                         select new string(unit.FacilityId + ";" + unit.UnitName))
                                     .ToList();
                        uvm.unitsMasterList = unitsList;
                        uvm.isActive = editUnit.isActive;
                    }
                    catch (Exception ex)
                    {
                        if (Constants.isAdmin)
                        {
                            _logger.LogError(ex.ToString());
                            return Content(Constants.GeneralExceptionAdminMessage);
                        }
                        else
                        {
                            _logger.LogError(ex.ToString());
                            return Content(Constants.GeneralExceptionUserMessage);
                        }
                    }

                    ViewBag.TitleMessage = "Edit Unit";
                    ViewBag.selectedOption = selection;
                    ViewBag.displayStatus = displayStatus;
                    return PartialView("~/Views/Units/_UnitPartial.cshtml", uvm);
                }


                [HttpPost]
                [ValidateAntiForgeryToken]
                public IActionResult SaveUnit(UnitsViewModel unitsModel, string displayStatus, string selectedOption)
                {
                bool validModel = ModelState.IsValid;
                    if (validModel)
                    {
                        try
                        {
                            if (unitsModel.UnitId != 0)
                            {
                                Units unitsVar = _UnitsDbContext.Units.Where(f => f.UnitId == unitsModel.UnitId).FirstOrDefault();
                                unitsVar.FacilityId = unitsModel.FacilityId;
                                unitsVar.UnitName = unitsModel.UnitName;
                                unitsVar.isActive = unitsModel.isActive;
                                _UnitsDbContext.SaveChanges();
                            }
                            else
                            {
                                Units unitsVar = new Units();
                                unitsVar.FacilityId = unitsModel.FacilityId;
                                unitsVar.UnitName = unitsModel.UnitName;
                                unitsVar.isActive = unitsModel.isActive;
                                _UnitsDbContext.Add(unitsVar);
                                _UnitsDbContext.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            if (Constants.isAdmin)
                            {
                                _logger.LogError(ex.ToString());
                                return Content(Constants.GeneralExceptionAdminMessage);
                            }
                            else
                            {
                                _logger.LogError(ex.ToString());
                                return Content(Constants.GeneralExceptionUserMessage);
                            }
                        }
                    }

                    ViewBag.selectedOption = selectedOption;
                    ViewBag.displayStatus = displayStatus;
                    return RedirectToAction("Index", "Units", new { status = displayStatus, selection = selectedOption });
                }

                [HttpPost]
                [ValidateAntiForgeryToken]
                public IActionResult DeleteUnit(string unitId, string displayStatus, string selection)
                {
                    try
                    {
                        int unitIdObj = Convert.ToInt32(unitId);
                        if (unitIdObj != 0)
                        {
                            Units units = _UnitsDbContext.Units.Where(f => f.UnitId == unitIdObj).FirstOrDefault();
                            _UnitsDbContext.Remove(units);
                            _UnitsDbContext.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        if (Constants.isAdmin)
                        {
                            _logger.LogError(ex.ToString());
                            return Content(Constants.GeneralExceptionAdminMessage);
                        }
                        else
                        {
                            _logger.LogError(ex.ToString());
                            return Content(Constants.GeneralExceptionUserMessage);
                        }
                    }
                    @ViewBag.successMessage = "Unit deleted Successfully";
                    ViewBag.selectedOption = selection;
                    ViewBag.displayStatus = displayStatus;
                    return RedirectToAction("Index", "Units", new { status = displayStatus, selection = selection });
                }

        public IActionResult ActivateUnit(string unitId, string status, string selection)
        {
            try
            {
                int convertedUnitId = Convert.ToInt32(unitId);
                if (convertedUnitId != 0)
                {
                    Units units = _UnitsDbContext.Units.Where(u => u.UnitId == convertedUnitId).FirstOrDefault();
                    units.isActive = true;

                    List<Drawings> drawings = _UnitsDbContext.Drawings.Where(d => d.UnitId == convertedUnitId).ToList();
                    foreach (var d in drawings)
                    {
                        d.isActive = true;
                    }

                    _UnitsDbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            return RedirectToAction("Index", "Units", new { status = status, selection = selection });
        }

        public IActionResult InactivateUnit(string unitId, string status, string selection)
        {
            try
            {
                int convertedUnitId = Convert.ToInt32(unitId);
                if (convertedUnitId != 0)
                {
                    Units units = _UnitsDbContext.Units.Where(u => u.UnitId == convertedUnitId).FirstOrDefault();
                    units.isActive = false;
                    List<Drawings> drawings = _UnitsDbContext.Drawings.Where(d => d.UnitId == convertedUnitId).ToList();
                    foreach (var d in drawings)
                    {
                        d.isActive = false;
                    }
                    _UnitsDbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            return RedirectToAction("Index", "Units", new { status = status, selection = selection });
        }

    }
}
