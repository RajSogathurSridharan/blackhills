﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using BlackHills.Models;
using BlackHills.ViewModel;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using DigitalLibrary;


namespace BlackHills.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _projectDbContext;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ILogger<HomeController> _logger;
        
        public HomeController(ApplicationDbContext projectDbContext, IMapper mapper, IConfiguration iconfiguration, ILogger<HomeController> logger)//, IWebHostEnvironment hostEnvironment)
        {
            _projectDbContext = projectDbContext;
            _mapper = mapper;
            _configuration = iconfiguration;
            _logger = logger;
        }

        public IActionResult Index()
        {
            if (User.Claims.Where(c => c.Type == "groups").Where(v => v.Value == _configuration.GetSection("Groups")["GroupAdmin"]).Count() != 0)
            {
                Constants.isAdmin = true;
            }
            try
            {
                DrawingsListModel BHDL = new DrawingsListModel();
                var facilitiesList = 
                 _projectDbContext.Facilities.Where(F => F.isActive == true).ToList()
                   .Select(X => new SelectListItem { Text = X.FacilityName, Value = X.FacilityId.ToString() });
                var facilitiesOrderedList = facilitiesList.ToList().OrderBy(F => F.Text);
                var facilititiesFinalList = new List<SelectListItem>();
                if (facilitiesOrderedList.Count() > 1)
                {
                    facilititiesFinalList.Add(new SelectListItem("Select", "0"));
                }
                foreach( var item in facilitiesOrderedList)
                {
                    facilititiesFinalList.Add(item);
                }
                
                BHDL.Facilities = facilititiesFinalList;
                IEnumerable<SelectListItem> unitsList = new List<SelectListItem>();
                BHDL.UnitsList = unitsList;
                return View(BHDL);
            }
            catch(Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                    
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                    
                }
            }
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(BlackHills.ViewModel.DrawingsListModel BHDL, string source, int facilityId, string DrawingName, string SystemDesc)
        {
            try
            {
                var facilitiesResult = _projectDbContext.Facilities.Where(F => F.isActive == true).OrderBy(F => F.FacilityName).ToList()
                    .Select(X => new SelectListItem { Text = X.FacilityName, Value = X.FacilityId.ToString() });
                BHDL.Facilities = getDropdownList(facilitiesResult.ToList(),
                    BHDL.FacilityId != null ? BHDL.FacilityId.ToString() : null);

                var unitsList = _projectDbContext.Units.Where(F => F.FacilityId == facilityId && F.isActive == true).OrderBy(U => U.UnitName).ToList()
                   .Select(X => new SelectListItem { Text = X.UnitName, Value = X.UnitId.ToString() });
                var unitsOrderedList = unitsList.ToList().OrderBy(F => F.Text);
                var unitsFinalList = new List<SelectListItem>();

                if (unitsOrderedList.Count() > 1)
                {
                    unitsFinalList.Add(new SelectListItem("All", "0"));
                }
                foreach (var item in unitsOrderedList)
                {
                    unitsFinalList.Add(item);
                }
                BHDL.UnitsList = unitsFinalList.ToList();

                var BlackHillListResult = (from d in _projectDbContext.Drawings
                                           join u in _projectDbContext.Units
                                           on d.UnitId equals u.UnitId
                                           where d.isActive == true && u.isActive == true
                                           select new DrawingsViewModel()
                                           {
                                               DrawingId = d.DrawingId,
                                               DrawingName = d.DrawingName,
                                               SystemDescription = d.SystemDescription,
                                               FilePath = d.FilePath,
                                               UnitId = d.UnitId,
                                               UnitDesc = u.UnitName
                                           }).ToList();


                var unitsValueList = (from u in _projectDbContext.Units
                                      where (u.FacilityId == BHDL.FacilityId)
                                      select u.UnitId).ToList();

                BlackHillListResult = BlackHillListResult.Where(U => unitsValueList.Contains(U.UnitId)).ToList();

                if (BHDL.UnitId != null && BHDL.UnitId != 0)
                {
                    BlackHillListResult = BlackHillListResult.Where(B => B.UnitId == BHDL.UnitId).ToList();
                }

                if (source != null && source.Equals("Search"))
                {

                    if (BHDL.searchDrawingName != null)
                    {
                        BlackHillListResult = BlackHillListResult.Where(B => B.DrawingName.ToUpper().Contains(BHDL.searchDrawingName.ToUpper())).ToList();
                    }

                    if (BHDL.searchSystemDesc != null)
                    {
                        BlackHillListResult = BlackHillListResult.Where(B => B.SystemDescription.ToUpper().Contains(BHDL.searchSystemDesc.ToUpper())).ToList();
                    }

                }


                var vm = from i in BlackHillListResult
                         select _mapper.Map<DrawingsViewModel>(i);
                BHDL.Data = vm.ToList();

                if (source != null && source.Equals("Reset"))
                {
                    BHDL.searchDrawingName = "";
                    BHDL.searchSystemDesc = "";
                    ModelState.Clear();
                }
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);

                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);

                }
            }
            return View(BHDL);
        }

        public List<SelectListItem> getDropdownList(List<SelectListItem> listItems, string selectedId)
        {

            List<SelectListItem> selectListItems = listItems.ConvertAll(a =>
            {
                if (selectedId != null && (a.Value == selectedId || a.Value.Equals(selectedId)))
                {
                    return new SelectListItem()
                    {
                        Text = a.Text.ToString(),
                        Value = a.Value,
                        Selected = true
                    };
                }
                else
                {
                    return new SelectListItem()
                    {
                        Text = a.Text.ToString(),
                        Value = a.Value,
                        Selected = false
                    };
                }
            });

            return selectListItems;
        }
       

        public FileStreamResult GetPDF(string FilePath)
        {
            FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
            return File(fs,"application/pdf");
        }

       

        public async Task<IActionResult> Download(string filename)
        {
            if (filename == null)
                return Content("filename not present");

            var memory = new MemoryStream();
            IConfiguration Configuration;
            try
            {
                using (var stream = new FileStream(filename, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
            }catch(DirectoryNotFoundException dnfe)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(dnfe.ToString());
                    return Content(Constants.FolderNotFoundAdminMessage);
                    
                }
                else
                {
                    _logger.LogError(dnfe.ToString());
                    return Content(Constants.FolderNotFoundUserMessage);
                    
                }

            }
            catch (FileNotFoundException fnfe)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(fnfe.ToString());
                    return Content(Constants.FileNotFoundAdminMessage);
                    
                }
                else
                {
                    _logger.LogError(fnfe.ToString());
                    return Content(Constants.FileNotFoundUserMessage);
                }


            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);

                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);

                }
            }
            memory.Position = 0;
            return File(memory, GetContentType(filename), Path.GetFileName(filename));
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }
        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
             {
                 {".txt", "text/plain"},
                 {".pdf", "application/pdf"},
                 {".doc", "application/vnd.ms-word"},
                 {".docx", "application/vnd.ms-word"},
                 {".xls", "application/vnd.ms-excel"},
                 {".xlsx", "application/vnd.openxmlformats officedocument.spreadsheetml.sheet"},
                 {".png", "image/png"},
                 {".jpg", "image/jpeg"},
                 {".jpeg", "image/jpeg"},
                 {".gif", "image/gif"},
                 {".csv", "text/csv"}
             };
        }


    }
}
