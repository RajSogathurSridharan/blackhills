﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlackHills.Models;
using BlackHills.ViewModel;
using DigitalLibrary;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

namespace BlackHills.Controllers
{
    public class FacilitiesController : Controller
    {

        private readonly ApplicationDbContext _FacilitiesDbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<FacilitiesController> _logger;

        public FacilitiesController(ApplicationDbContext projectDbContext, IMapper mapper, ILogger<FacilitiesController> logger)
        {
            _FacilitiesDbContext = projectDbContext;
            _mapper = mapper;
            _logger = logger;
        }


        public IActionResult Index(string status, string selection)
        {
            FacilitiesListModel FLM = new FacilitiesListModel();

            try
            {
                // query to pull only the facility List that have associated drwaings
                var facilitiesListWithDrawings = (from fac in _FacilitiesDbContext.Facilities
                                                  join unit in _FacilitiesDbContext.Units
                                                  on fac.FacilityId equals unit.FacilityId
                                                  join draw in _FacilitiesDbContext.Drawings
                                                  on unit.UnitId equals draw.UnitId
                                                  select new FacilitiesViewModel { FacilityId = fac.FacilityId, FacilityName = fac.FacilityName, isActive = fac.isActive }).Distinct().ToList();

                // query to identify if the facilities has drawings or not and set the deleteEnabledFlag accordingly in the facilityList
                var facilityList = (from fac in _FacilitiesDbContext.Facilities.ToList()
                                    join facLst in facilitiesListWithDrawings
                                    on fac.FacilityId equals facLst.FacilityId
                                    into facObject
                                    from facObj in facObject.DefaultIfEmpty()
                                    select new FacilitiesViewModel { FacilityId = fac.FacilityId, FacilityName = fac.FacilityName, deleteEnabledFlag = facObj != null ? false : true, isActive = fac.isActive }).GroupBy(f => f.FacilityId)
                                         .Select(g => g.FirstOrDefault()).AsQueryable();

                if (status == null)
                {
                    status = "true";
                }

                var facilitiesFilteredList = facilityList.Where(D => D.isActive == bool.Parse(status)).OrderBy(F => F.FacilityName).ToList();

                FLM.Data = facilitiesFilteredList.ToList();

            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);
                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);
                }
            }
            ViewBag.selectedOption = selection;
            ViewBag.displayStatus = status;
            return View(FLM);
        }

        public IActionResult AddFacility(string displayStatus, string selection)
        {
            FacilitiesViewModel FVM = new FacilitiesViewModel();
            ViewBag.TitleMessage = "Add Facility";
            try
            {
                FVM.isActive = true;
                var facilitiesList = (from fac in _FacilitiesDbContext.Facilities
                                      select new string(fac.FacilityName))
                                     .ToList();
                FVM.facilitiesNamesList = facilitiesList;
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);

                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);

                }
            }
            ViewBag.selectedOption = selection;
            ViewBag.displayStatus = displayStatus;
            return PartialView("~/Views/Facilities/_FacilityPartial.cshtml", FVM);
        }

        public IActionResult EditFacility(string facilityId, string facilityName, string displayStatus, string selection)
        {
            FacilitiesViewModel fvm = new FacilitiesViewModel();
            try
            {
                var editFacility = _FacilitiesDbContext.Facilities.Where(f => f.FacilityId == Convert.ToInt32(facilityId)).FirstOrDefault();
                fvm.FacilityId = Convert.ToInt32(facilityId);
                fvm.FacilityName = editFacility.FacilityName;
                ViewBag.TitleMessage = "Edit Facility";
                var facilitiesList = (from fac in _FacilitiesDbContext.Facilities
                                      where (fac.FacilityId != int.Parse(facilityId))
                                      select new string(fac.FacilityName))
                                     .ToList();
                fvm.facilitiesNamesList = facilitiesList;
                fvm.isActive = editFacility.isActive;
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);

                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);

                }
            }
            ViewBag.selectedOption = selection;
            ViewBag.displayStatus = displayStatus;
            return PartialView("~/Views/Facilities/_FacilityPartial.cshtml", fvm);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveFacility(FacilitiesViewModel facilitiesModel, string displayStatus, string selectedOption)
        {
            bool validModel = ModelState.IsValid;
            if (validModel)
            {
                try
                {
                    var facilitiesList = _FacilitiesDbContext.Facilities.ToList();

                    if (facilitiesModel.FacilityId != 0)
                    {
                        Facilities facilities = facilitiesList.Where(f => f.FacilityId == facilitiesModel.FacilityId).FirstOrDefault();
                        facilities.FacilityName = facilitiesModel.FacilityName;
                        facilities.isActive = facilitiesModel.isActive;
                        _FacilitiesDbContext.SaveChanges();
                    }
                    else
                    {
                        Facilities facilities = new Facilities();
                        facilities.FacilityName = facilitiesModel.FacilityName;
                        facilities.isActive = facilitiesModel.isActive;
                        _FacilitiesDbContext.Add(facilities);
                        _FacilitiesDbContext.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    if (Constants.isAdmin)
                    {
                        _logger.LogError(ex.ToString());
                        return Content(Constants.GeneralExceptionAdminMessage);

                    }
                    else
                    {
                        _logger.LogError(ex.ToString());
                        return Content(Constants.GeneralExceptionUserMessage);

                    }
                }

            }
            ViewBag.selectedOption = selectedOption;
            ViewBag.displayStatus = displayStatus;
            return RedirectToAction("Index", "Facilities", new { status = displayStatus, selection = selectedOption });



        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteFacility(string facilityId, string displayStatus, string selection)
        {

            int facId = Convert.ToInt32(facilityId);
            try
            {
                if (facId != 0)
                {
                    List<Units> units = _FacilitiesDbContext.Units.Where(u => u.FacilityId == facId).ToList();
                    _FacilitiesDbContext.RemoveRange(units);
                    _FacilitiesDbContext.SaveChanges();
                    Facilities facilities = _FacilitiesDbContext.Facilities.Where(f => f.FacilityId == facId).FirstOrDefault();
                    _FacilitiesDbContext.Remove(facilities);
                    _FacilitiesDbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (Constants.isAdmin)
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionAdminMessage);

                }
                else
                {
                    _logger.LogError(ex.ToString());
                    return Content(Constants.GeneralExceptionUserMessage);

                }
            }
            ViewBag.successMessage = "Facility deleted successfully.";
            ViewBag.selectedOption = selection;
            ViewBag.displayStatus = displayStatus;
            return RedirectToAction("Index", "Facilities", new { status = displayStatus, selection = selection });
        }

        public IActionResult ActivateFacility(string facilityId, string status, string selection)
        {

            int facId = int.Parse(facilityId);
            if (facId != 0)
            {
                try
                {
                    Facilities facilities = _FacilitiesDbContext.Facilities.Where(f => f.FacilityId == facId).FirstOrDefault();
                    facilities.isActive = true;

                    List<Units> units = _FacilitiesDbContext.Units.Where(u => u.FacilityId == facId).ToList();
                    foreach (var u in units)
                    {
                        u.isActive = true;
                        List<Drawings> drawings = _FacilitiesDbContext.Drawings.Where(d => d.UnitId == u.UnitId).ToList();
                        foreach (var d in drawings)
                        {
                            d.isActive = true;
                        }
                    }
                    _FacilitiesDbContext.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (Constants.isAdmin)
                    {
                        _logger.LogError(ex.ToString());
                        return Content(Constants.GeneralExceptionAdminMessage);

                    }
                    else
                    {
                        _logger.LogError(ex.ToString());
                        return Content(Constants.GeneralExceptionUserMessage);

                    }
                }

            }
            return RedirectToAction("Index", "Facilities", new { status = status, selection = selection });
        }

        public IActionResult InactivateFacility(string facilityId, string status, string selection)
        {

            int facId = int.Parse(facilityId);
            if (facId != 0)
            {
                try
                {
                    Facilities facilities = _FacilitiesDbContext.Facilities.Where(f => f.FacilityId == facId).FirstOrDefault();
                    facilities.isActive = false;

                    List<Units> units = _FacilitiesDbContext.Units.Where(u => u.FacilityId == facId).ToList();
                    foreach (var u in units)
                    {
                        u.isActive = false;
                        List<Drawings> drawings = _FacilitiesDbContext.Drawings.Where(d => d.UnitId == u.UnitId).ToList();
                        foreach (var d in drawings)
                        {
                            d.isActive = false;
                        }
                    }
                    _FacilitiesDbContext.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (Constants.isAdmin)
                    {
                        _logger.LogError(ex.ToString());
                        return Content(Constants.GeneralExceptionAdminMessage);

                    }
                    else
                    {
                        _logger.LogError(ex.ToString());
                        return Content(Constants.GeneralExceptionUserMessage);

                    }
                }
            }
            return RedirectToAction("Index", "Facilities", new { status = status, selection = selection });
        }
    }
}
